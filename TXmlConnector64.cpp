#include "pro_belbix_finam_TXmlConnector64.h"
#include <windows.h>
#include <winbase.h>
#include <iostream>
#include <string.h>
#include <cstdlib> 
#include <chrono>
#include <thread>
#include <assert.h> 
#include "tchar.h"
#include  "limits.h"


using namespace std;
#define JNI_TRUE 1 

typedef bool(*tcallback)(BYTE* pData);			 // tcallback	- ������� ��������� ������
typedef bool(*tcallbackEx)(BYTE* pData, void*);  // tcallbackEx - C ������� ������� SetCallbackEx ����� �������� ���������� ��������� �� ������ ������������, ������� ����� ������������ � callback ������� ��� ������ �� ������. 

//HMODULE connector = LoadLibrary(TEXT("C:\\workspace\\finam\\target\\classes\\txmlconnector64.dll")); // ���� txmlconnector64.dll ������ ������ ����� � ��������
HMODULE connector;
JavaVM *cachedJVM;
jobject cbObject = NULL;
jmethodID cbMid = NULL;
jint JNI_VERSION;

/*	���������� �������
��������� ������������ ��������� �������(���������� ������ __stdcall) :
*/
bool	SetCallbackEx(tcallbackEx pCallbackEx, void* userData);
bool    FreeMemory(BYTE* pData);
bool   Callback(BYTE* data);

/* ��������� ������ */
bool validateDLL();
BYTE* charToByte(const char* someString, int size);
jbyteArray byteToJbyteArray(JNIEnv * env, BYTE * b, int size, bool simple = false);
int sizeAnswer(BYTE * b, int size);
bool findEnd(BYTE* arr, int i, int j);
BYTE* charPathToByte(const char* someString, int size);
int arrayFindChar(const jchar * path, BYTE c);
char* byteToChar(BYTE* bytes, int size);

/* ����������� ������� */
JNIEXPORT jboolean JNICALL Java_pro_belbix_finam_TXmlConnector64_initDll(JNIEnv * env, jobject obj, jstring path) {
	const jchar* carr = (env)->GetStringChars(path, false);
	int size = env->GetStringUTFLength(path);

	if (carr == NULL) {
		return false;
	}

	//print_jchar("Path to dll:", carr, size);

	connector = LoadLibrary((LPCWSTR)carr);

	(env)->ReleaseStringChars(path, carr);
	return validateDLL();
}

/*
* ��������� ������������� ����������:
* - ��������� ����� ��������� ������� �������� �������;
* - �������������� ������� ����������� ����������.
* ���������:
* - logPath	 - ���� � ����������, � ������� ����� ����������� ����� �������
* - logLevel - ������� �����������
*/
JNIEXPORT jbyteArray JNICALL Java_pro_belbix_finam_TXmlConnector64_Initialize(JNIEnv * env, jobject obj, jstring path, jint logLevel) {
	if (!validateDLL()) {
		return 0;
	}

	const char* jpath = (env)->GetStringUTFChars(path, 0);
	int size = env->GetStringUTFLength(path);
	if (jpath == NULL) {
		return 0;
	}

	const BYTE* PATH = charPathToByte(jpath, size);
	BYTE* (*initMethod)(const BYTE* logPath, int logLevel);
	(FARPROC&)initMethod = GetProcAddress(connector, "Initialize");
	BYTE* answer = initMethod(PATH, logLevel);
	bool suc = true;

	//---------------print path--------------------------------------
	/*char * msgConcat = new char;
	char msg[47] = "<success>INITIALIZATION SUCCESSFUL!</success>\0";
	int msgSize;
	if (answer == NULL) {
		msgSize = 47 + size;
		strcpy(msgConcat, "<answer>\0");
		strcat(msgConcat, msg);
		strcat(msgConcat, jpath);
		strcat(msgConcat, "</answer>\0");
		msgSize += 21;
		suc = true;
	}
	else {
		int asize = sizeAnswer(answer, 500) + 1;
		char * t = byteToChar(answer, asize);
		strcpy(msgConcat, "<answer>\0");
		strcat(msgConcat, t);
		strcat(msgConcat, jpath);
		strcat(msgConcat, "</answer>\0");

		msgSize = asize + size;
		msgSize += 21;
	}

	answer = charToByte(msgConcat, msgSize);*/

	//---------------non print path--------------------------------------
	if (answer == NULL) {
		char msg[100] = "<success>INITIALIZATION SUCCESSFUL!</success>";
		answer = charToByte(msg, sizeof(msg));
		suc = 1;
	}


	jbyteArray res = byteToJbyteArray(env, answer, 500);
	(env)->ReleaseStringUTFChars(path, jpath);
	if (!suc) {
		FreeMemory(answer);
	}

	//delete PATH;
	return res;
}

/*
*	������������� ��������� �� ������� ��������� ������ �������,
*	������� ����� ��������� ����������� �������������� ��������� �� ����������.
*/
JNIEXPORT jboolean JNICALL Java_pro_belbix_finam_TXmlConnector64_SetCallback(JNIEnv * env, jobject obj, jobject callback)
{
	if (!validateDLL()) {
		return false;
	}

	jclass callbackClass = env->FindClass("pro/belbix/finam/Callback");
	if (callbackClass == NULL) {
		return false;
	}

	jmethodID mid = env->GetStaticMethodID(callbackClass, "callback", "([B)V");
	if (mid == NULL) {
		return false;
	}

	
	cbObject = callback;
	env->NewGlobalRef(cbObject);
	cbMid = mid;
	int status = env->GetJavaVM(&cachedJVM);
	if (status != 0) {
		return false;
	}

	JNI_VERSION = env->GetVersion();

	bool(*setCallback)(tcallback pCallback);
	(FARPROC&)setCallback = GetProcAddress(connector, "SetCallback");
	return setCallback(Callback);
}

bool Callback(BYTE* pData) {
	if (pData != NULL && cbObject != NULL && cbMid != NULL) {
		try {
			JNIEnv * cbEnv = NULL;

			int status = cachedJVM->AttachCurrentThread((void **)&cbEnv, NULL);

			if (status != 0) {
				return false;
			}

			int get_status = cachedJVM->GetEnv((void **)&cbEnv, JNI_VERSION);

			if (get_status != 0) {
				return false;
			}

			jbyteArray res = byteToJbyteArray(cbEnv, pData, 5000);
			if (res != NULL) {
				cbEnv->CallVoidMethod(cbObject, cbMid, res);
				
				if (cbEnv->ExceptionCheck()) {
					cbEnv->ExceptionClear();

					char * msg = "Exeption invoke callback method";
					if (cachedJVM == NULL) {
						msg = "Exeption invoke callback method, cachedJVM = NULL";
					}
					if (cbEnv == NULL) {
						msg = "Exeption invoke callback method, cbEnv = NULL";
					}
					if (cbObject == NULL) {
						msg = "Exeption invoke callback method, cbObject = NULL";
					}
					if (cbMid == NULL) {
						msg = "Exeption invoke callback method, cbMid = NULL";
					}

					jclass jc = cbEnv->FindClass("java/lang/Exception");
					if (jc) cbEnv->ThrowNew(jc, msg);
				}

			}

			cachedJVM->DetachCurrentThread();

			FreeMemory(pData);
		}
		catch (exception) {}
		return true;
	}
	else {
		return false;
	}
}

JNIEXPORT jbyteArray JNICALL Java_pro_belbix_finam_TXmlConnector64_SendCommand(JNIEnv * env, jobject obj, jstring cmd) {
	const char* ccmd = (env)->GetStringUTFChars(cmd, 0);
	int size = env->GetStringUTFLength(cmd);

	BYTE* pData = charToByte(ccmd, size);

	BYTE* (*sendCommand)(BYTE* pData);
	(FARPROC&)sendCommand = GetProcAddress(connector, "SendCommand");
	BYTE* res = sendCommand(pData);


	jbyteArray ret = byteToJbyteArray(env, res, 500);
	(env)->ReleaseStringUTFChars(cmd, ccmd);
	FreeMemory(res);

	delete pData;
	return ret;
}

JNIEXPORT jbyteArray JNICALL Java_pro_belbix_finam_TXmlConnector64_UnInitialize(JNIEnv * env, jobject obj) {
	BYTE*(*unInitialize)();
	(FARPROC&)unInitialize = GetProcAddress(connector, "UnInitialize");
	BYTE* res = unInitialize();

	bool suc = false;
	if (res == NULL) {
		char msg[100] = "<success>UNINITIALIZATION SUCCESSFUL!</success>";
		res = charToByte(msg, sizeof(msg));
		suc = true;
	}

	jbyteArray ret = byteToJbyteArray(env, res, 500);

	if (!suc) {
		FreeMemory(res);
	}
	env->DeleteGlobalRef(cbObject);
	FreeLibrary(connector);
	return ret;
}

bool FreeMemory(BYTE* pData) {
	bool(*freeMemory)(BYTE* pData);
	(FARPROC&)freeMemory = GetProcAddress(connector, "FreeMemory");
	return freeMemory(pData);
}

/*
*	������������� ��������� �� ������� ��������� ������ �������,
*	������� ����� ��������� ����������� �������������� ��������� �� ����������.
*	C ������� ������� SetCallbackEx ����� �������� ���������� ��������� �� ������ ������������
*	(��������, ������-������������� ����������, �����-���� ���������������� ������),
*	������� ����� ������������ � callback ������� ��� ������ �� ������.
*/
bool SetCallbackEx(tcallbackEx pCallbackEx, void* userData) {
	bool(*setCallbackEx)(tcallbackEx pCallbackEx, void* userData);
	(FARPROC&)setCallbackEx = GetProcAddress(connector, "SetCallbackEx");

	return setCallbackEx(pCallbackEx, userData);
}

bool validateDLL() {
	return connector != NULL;
}

int sizeAnswer(BYTE * b, int size) {
	int i = 0;
	bool run = true;
	bool find = false;
	int ifound = 0;
	int end_name = 0;
	BYTE key_space = (BYTE)' ';
	BYTE key_endtag = (BYTE)'>';
	BYTE key_end1 = (BYTE)'/';
	BYTE key_start1 = (BYTE)'<';
	BYTE key_start2 = (BYTE)' ';
	bool spaceFinded = false;
	if (b[0] == key_start1 && b[1] != key_start2) {

		while (run && (i < size))
		{
			if (!find) {
				if (b[i] == key_endtag) {
					if (b[i - 1] == key_end1) {
						run = false;
					}
					else {
						find = true;
						if (end_name == 0) {
							end_name = i;
						}
					}
				}
				if (!spaceFinded && b[i] == key_space) {
					end_name = i;
					spaceFinded = true;
				}

			}
			else {
				if (findEnd(b, i, end_name - 1)) {
					run = false;
				}
			}
			i++;
		}

		size = i + 1;
	}

	return size;
}

jbyteArray byteToJbyteArray(JNIEnv * env, BYTE * b, int size, bool simple) {
	if (!simple) {
		size = sizeAnswer(b, size);
	}
	jbyteArray res = env->NewByteArray(size);
	env->SetByteArrayRegion(res, 0, size, (jbyte*)b);
	return res;
}

bool findEnd(BYTE* arr, int i, int j) {
	BYTE key_end1 = (char)'/';
	BYTE key_end2 = (char)'<';
	if (arr[i] == arr[j]) {
		if (j == 1 && arr[i - 1] == key_end1 && arr[i - 2] == key_end2) {
			return true;
		}
		else
		{
			return findEnd(arr, i - 1, j - 1);
		}
	}
	else {
		return false;
	}
}

BYTE* charPathToByte(const char* someString, int size) {
	BYTE* byteStr = (BYTE*)someString;
	char zero = '\0';
	byteStr[size + 1] = (BYTE)zero;
	return byteStr;
}

/*
*  ��������������� �����.
*  ���������� ��������� �� ������ � ������ �������
*  @return BYTE* - unsigned char *
*/
BYTE* charToByte(const char* someString, int size) {
	BYTE* byteStr = new BYTE[size + 1];
	strcpy_s((char*)byteStr, size + 1, someString);

	return byteStr;
}

char* byteToChar(BYTE* bytes, int size) {
	char * carr = new char;

	for (int i = 0; i < size; i++) {
		carr[i] = (char)bytes[i];
	}

	return carr;
}

int arrayFindChar(const jchar * path, BYTE c) {
	int i = 0;
	bool run = true;
	cout << "path:";
	while (run && (i < MAX_PATH))
	{
		//res[i] = path[i];
		cout << (char)path[i];
		if (c == (char)path[i]) {
			run = false;
		}
		i++;
	}
	cout << "\n";
	return i;
}

